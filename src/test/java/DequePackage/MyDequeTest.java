package DequePackage;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyDequeTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() throws DequeException{
        MyDeque deque = new MyDeque();
    }

    @Test
    void testPushBack() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.pushBack(2);
        assertEquals(2, deque.back());
    }

    @Test
    private void testPushFront() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.pushFront(1);
        assertEquals(1, deque.front());
    }

    @Test
    void testPopFront() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.popFront();
        assertEquals("error", deque.popFront());
    }

    @Test
    void testPopBack() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.popBack();
        assertEquals("error", deque.popBack());
    }

    @Test
    void testFront() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.pushFront(1);
        deque.front();
        assertEquals(1, deque.front());
    }

    @Test
    void testBack() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.pushBack(2);
        deque.back();
        assertEquals(2, deque.back());
    }

    @Test
    void testGetSize() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.pushFront(1);
        deque.pushBack(2);
        deque.getSize();
        assertEquals(2, deque.getSize());
    }

    @Test
    void testClear() throws DequeException {
        MyDeque deque = new MyDeque();
        deque.pushFront(1);
        deque.pushBack(2);
        deque.clear();
        assertEquals(0, deque.getSize());
    }

    @Test
    void testEx() {
        MyDeque deque = new MyDeque();
        deque.ex();
        assertEquals("bye", deque.ex());
    }
}