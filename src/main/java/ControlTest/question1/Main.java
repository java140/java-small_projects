package ControlTest.question1;

import ControlTest.question1.service.CarOperation;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Mark> markList = Arrays.asList(
            new Mark("BMW",  Arrays.asList(
                    new Cars("E-46", 2001, 5000, 12.5, 3,"Yes"),
                    new Cars("X6M", 2019, 150000, 0.3, 9, "Yes")

            )),
                new Mark("Audi", Arrays.asList(
                        new Cars ("TT", 2004, 12000, 3, 2,"Nope"),
                        new Cars("R8", 2003, 104000, 4, 5,"Nope")
                )),

                new Mark("KAMAZ", Arrays.asList(
                        new Cars("01", 1984, 10000, 150.4, 12, "Nope"),
                        new Cars("02", 1989, 12000, 250.4, 16,"Yes")

                ))
        );
        CarOperation groupOperations = new CarOperation();
        System.out.println(groupOperations.afterCrash(markList));


    }

}
