package ControlTest.question1.service;

import ControlTest.question1.Cars;
import ControlTest.question1.Mark;

import java.util.List;
import java.util.stream.Collectors;

public class CarOperation {
    public List<Cars> afterCrash(List<Mark> F) {
        return F.stream()
                .flatMap(group -> group.getCars().stream())
                .filter(st -> st.getCrash().equals("Yes"))
                .collect(Collectors.toList());
    }

}
