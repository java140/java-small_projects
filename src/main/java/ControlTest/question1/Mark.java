package ControlTest.question1;

import ControlTest.question1.Cars;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Mark implements Serializable {
    private String name;
    private List<Cars> cars;
}
