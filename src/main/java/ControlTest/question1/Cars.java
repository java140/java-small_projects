package ControlTest.question1;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Cars implements Serializable {
    private String pName; // personal name
    private int year;
    private int price;
    private double mileage;
    private double weight;
    private String crash; //car after the crash
}
