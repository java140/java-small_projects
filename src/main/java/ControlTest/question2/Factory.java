package ControlTest.question2;

interface Factory{
    Audi createAudi(String typeOfAudi);
    Mercedes createMercedes(String typeOfMercedes);
}
