package ControlTest.question2;

public class AbstractFactory {
    Factory createFactory(String typeOfFactory){
        switch (typeOfFactory){
            case "Audi": return new AudiFactory();
            case "Mercedes": return new MercedesFactory();
            default: return null;
        }
    }
}
