package ControlTest.question2;

public class AudiFactory implements Factory{
    public Audi createAudi(String typeOfAudi){
        switch (typeOfAudi){
            case "TT" : return new Tt();
            case "R8" : return new R8();
            default: return null;
        }
    }

    @Override
    public Mercedes createMercedes(String typeOfMercedes) {
        return null;
    }
}
