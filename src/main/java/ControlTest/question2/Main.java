package ControlTest.question2;


public class Main {
    public static void main(String[] args) {
        Factory audiFactory = new AbstractFactory().createFactory("Audi");
        Factory mercedesFactory = new AbstractFactory().createFactory("Mercedes");
        Audi tt = audiFactory.createAudi("TT");
        Audi r8 = audiFactory.createAudi("R8");
        r8.isCar();
        tt.isCar();
        Mercedes a_class = mercedesFactory.createMercedes("A-class");
        Mercedes c_class = mercedesFactory.createMercedes("C-class");
        a_class.isCar();
        c_class.isCar();
    }
}





