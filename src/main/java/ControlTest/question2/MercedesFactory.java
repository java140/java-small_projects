package ControlTest.question2;

public class MercedesFactory implements Factory{
    public Mercedes createMercedes(String typeOfMercedes){
        switch (typeOfMercedes){
            case "A-class": return new A_class();
            case "C-class": return new C_class();
            default: return null;
        }
    }

    @Override
    public Audi createAudi(String typeOfAudi) {
        return null;
    }
}
