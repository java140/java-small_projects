package patern.factory;

public class FactoryCars {
    
    public static void main(String[] args) {
        Factory factory = new Factory();
        Cars mercedes = factory.create("Mercedes");
        Cars audi = factory.create("Audi");
        mercedes.drive();
        audi.drive();
    }
}


