package patern.factory;

class Audi implements Cars{

    @Override
    public void drive() {
        System.out.println("Drive Audi");
    }
}
