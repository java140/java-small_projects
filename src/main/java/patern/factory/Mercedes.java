package patern.factory;

class Mercedes implements Cars{

    @Override
    public void drive() {
        System.out.println("Drive Mercedes");
    }
}
