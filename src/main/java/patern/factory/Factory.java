package patern.factory;

class Factory {
    public Cars create(String typeOfCars) {
        switch (typeOfCars){
            case "Mercedes": return new Mercedes();
            case "Audi": return new Audi();
            default: return null;
        }
    }
}
