package patern.builder;

import lombok.*;

public class Builder {
    public static void main(String[] args) {
        SportCars sportCars = new SportCars.Builder("VAZ-2107").setColor("Blue").setMaxSpeed(190).build();
        System.out.println(sportCars.getName());
        System.out.println(sportCars.getColor());
        System.out.println(sportCars.getMaxSpeed());

    }
}

