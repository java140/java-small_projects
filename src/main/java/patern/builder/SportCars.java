package patern.builder;

import lombok.Getter;

class SportCars {
    @Getter
    private String name;
    @Getter
    private String color;
    @Getter
    private int maxSpeed;

    private SportCars(Builder builder) {
        this.name = builder.name;
        this.color = builder.color;
        this.maxSpeed = builder.maxSpeed;


    }

    static class Builder {
        private String name;
        private String color;
        private int maxSpeed;

        public Builder(String name) {
            this.name = name;
        }

        public Builder setColor(String color) {
            this.color = color;
            return this;
        }

        public Builder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public SportCars build() {
            return new SportCars(this);
        }
    }
}
