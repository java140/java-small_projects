package University;

import lombok.*;

import javax.security.auth.Subject;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public  class Student implements Serializable {
    private String firstName;
    private String surName;
    private String sex;
    private int age;
    private List<Integer> scores;
//    private Map<Subject, Integer> subjectMap;
//
//
//    public Map<Subject, Integer> getScores() {return subjectMap;}


//    @Override
//    public int compareTo(Student student) {
//        Double st1 = this.getScores().values().stream().mapToInt(i -> i).average().getAsDouble();
//        Double st2 = student.getScores().values().stream().mapToInt(i -> i).average().getAsDouble();
//        return st2.compareTo(st1);
//        }
}