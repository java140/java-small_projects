package University;

import University.service.GroupOperations;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {


    public static void main(String[] args) {

        List<Group> groups = Arrays.asList(
          new Group("101", Arrays.asList(
                  new Student("Oscar", "Walker", "Male", 18, Arrays.asList(95, 75, 88)),
                  new Student("Lola", "Brown", "Female", 19, Arrays.asList(60, 60, 60)),
                  new Student("Kenny", "Rice", "Male", 19, Arrays.asList(72, 75, 100)),
                  new Student("Carmella", "Ricky", "Female", 20, Arrays.asList(94, 99, 91))
          )),
          new Group("102", Arrays.asList(
                  new Student("Donald", "Tramp", "Male", 20, Arrays.asList(93, 66, 100)),
                  new Student("Karina", "Brance", "Female", 20, Arrays.asList(67, 67, 99)),
                  new Student("Alexandra", "Mia", "Female", 30, Arrays.asList(88, 67, 76)),
                  new Student("Julianna", "Lopez", "Female", 35, Arrays.asList(82, 93, 65))
          )),
          new Group("103", Arrays.asList(
                new Student("Rick", "Johnson", "Male", 28, Arrays.asList(67, 86, 100)),
                new Student("John", "Jefferson", "Male", 23, Arrays.asList(93, 61, 99)),
                new Student("Kim", "Riv", "Male", 20, Arrays.asList(88, 67, 89)),
                new Student("Don", "Soto", "Male", 19, Arrays.asList(88, 93, 100))
          )),
           new Group("104", Arrays.asList(
                   new Student("Tara", "Lopez", "Female", 20, Arrays.asList(100, 99, 100)),
                   new Student("Lo", "Li", "Female", 21, Arrays.asList(77, 76, 89)),
                   new Student("Niko", "Robin", "Female", 24, Arrays.asList(100, 100, 60)),
                   new Student("Nami", "Okumoto", "Female", 21, Arrays.asList(100, 100, 100))
           ))
        );

        GroupOperations groupOperations = new GroupOperations();
        System.out.println(groupOperations.studentsSex(groups));
    }
}
