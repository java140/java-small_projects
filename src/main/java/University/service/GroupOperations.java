package University.service;

import University.Faculty;
import University.Group;
import University.Student;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

public class GroupOperations {
  public List<Student> studentsSex (List <Group> F){
        return F.stream()
                .flatMap(group -> group.getStudents().stream())
                    .filter(st -> st.getSex().equals("Female"))
                        .collect(Collectors.toList());
    }

   public List<Student> bestStudet(List <Group> F){
      return F.stream()
              .flatMap(group -> group.getStudents().stream())
              .sorted().skip(2).limit(2).collect(Collectors.toList());
   }


}
