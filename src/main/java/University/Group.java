package University;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Group implements Serializable {
    private String name;
    private List<Student> students;

}
